/**
 * @author Arthur Chafonov <actuosus@gmail.com>
 * @fileoverview Webpack 3 project build configuration
 * Date: 01/02/18
 */

/*jslint white: true, single: true, node: true */
/*jshint esversion: 6 */

'use strict';

const path = require('path');
const webpack = require('webpack');
const TidyHtmlWebpackPlugin = require('tidy-html-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const env = process.env.ENVIRONMENT || process.env.NODE_ENV || 'development';

const sourceDir = path.resolve(__dirname, 'src');
const scriptsDir = path.resolve(sourceDir, 'scripts');
const outputDir = path.resolve(__dirname, 'build');

const API_URL = '/api/v1.0/gallery/rcc/mapt';
const CLOUDINARY_URL = '//res.cloudinary.com/interlikeapp/image/fetch/t_prv270/';
const CLOUDINARY_HOST = 'https://beta.interlikeapp.com';

/**
 * Webpack configuration
 *
 * @type {Object}
 */
const config = {
  entry: {
    bundle: path.resolve(scriptsDir, 'main.js')
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [ {
          loader: 'html-loader',
          options: {
            minimize: env === 'production'
          }
        }],
      },
      {
        test: /\.js$/,
        include: scriptsDir,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              plugins: ['transform-runtime'],
              sourceMap: env === 'development'
            }
          }
        ]
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract(
          {
            fallback: 'style-loader',
            use: [{
              loader: 'css-loader',
              options: {
                sourceMap: env === 'development'
              }
            }]
          }
        )
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: 'images/[name].[ext]'
          }
        }
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/,
        loader: 'image-webpack-loader',
        // Specify enforce: 'pre' to apply the loader
        // before url-loader/svg-url-loader
        // and not duplicate it in rules with them
        enforce: 'pre'
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'url-loader',
        options: {
          // Images larger than 10 KB won’t be inlined
          limit: 10 * 1024
        }
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader',
        options: {
          // Images larger than 10 KB won’t be inlined
          limit: 10 * 1024,
          // Remove quotes around the encoded URL
          noquotes: true
        }
      },
      {
        test: /\.(eot|woff|woff2|ttf)$/i,
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]'
          }
        }
      }
    ]
  },
  output: {
    path: outputDir,
    publicPath: './',
    filename: '[name].js',
    chunkFilename: '[chunkhash].js'
  },
  resolve: {
    modules: [sourceDir, 'node_modules'],
  },
  plugins: [
    new webpack.DefinePlugin({
      API_URL: JSON.stringify(API_URL),
      CLOUDINARY_URL: JSON.stringify(CLOUDINARY_URL),
      CLOUDINARY_HOST: JSON.stringify(CLOUDINARY_HOST)
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common'
    }),
    new TidyHtmlWebpackPlugin({
      tidy: {
        hideComments: env === 'production',
        indent: env === 'development',
        tabSize: 4
      },
      disabled: false
    }),
    new HtmlWebpackPlugin({
      filename: path.resolve(outputDir, 'index.html'),
      favicon: path.resolve(sourceDir, 'favicon.png'),
      template: path.resolve(sourceDir, 'index.html'),
      minify: env === 'production' ? {
        collapseWhitespace: true,
        collapseBooleanAttributes: true,
        decodeEntities: true,
        sortAttributes: true,
        sortClassName: true,
        removeComments: true,
        removeRedundantAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true,
        useShortDoctype: true
      } : false,
      hash: true
    }),
    new ExtractTextPlugin({
      filename: '[name].css'
    }),
    // new CopyWebpackPlugin([
    //   {
    //     from: path.resolve(sourceDir, 'images'),
    //     to: path.resolve(outputDir, 'images'),
    //   }
    // ])
  ],
  devtool: 'source-map',
  devServer: {
    hot: true,
    contentBase: path.resolve('build'),
    watchContentBase: true,
    quiet: false,
    open: true,
    proxy: {
      '/api': {
        target: 'https://beta.interlikeapp.com',
        secure: false,
        logLevel: 'debug',
        changeOrigin: true
      },
      '/published': {
        target: 'https://beta.interlikeapp.com',
        secure: false,
        logLevel: 'debug',
        changeOrigin: true
      }
    }
  }
};

if (env === 'production') {
  config.plugins.push(new webpack.optimize.UglifyJsPlugin({
    comments: false,
    compress: {
      drop_console: true
    }
  }));
}

if (env === 'development') {
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
  config.plugins.push(new webpack.NamedModulesPlugin());
}

module.exports = Object.assign({}, config);
