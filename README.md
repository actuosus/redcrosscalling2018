# Red Cross Map

Static web project for Red Cross Calling 2018.

## Build

Build for production:
```bash
yarn run build
```

Build for development:
```bash
yarn run build:dev
```

Static files will be built to `build` folder.

## Development

Start Webpack Development Server:
```bash
yarn run dev
```
