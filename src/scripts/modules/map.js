/**
 * @author Arthur Chafonov <actuosus@gmail.com>
 * @fileoverview
 * @version 1.0.0
 * @module
 */

import * as turf from '@turf/turf/index';
import L from 'leaflet';
import $ from 'jquery';


export const ICON_SIZE = 8;
export const ICON_GROW_STEP = 4;
const MAX_SEED_STATE_POINT_NUMBER = 20;
const MAX_SEED_CITY_POINT_DISTANCE = 100;

export function iconForMarker(iconSize = ICON_SIZE, html = '') {
  return L.divIcon({
    iconSize: [iconSize, iconSize],
    iconAnchor: [iconSize / 2, iconSize / 2],
    popupAnchor: [10, 0],
    shadowSize: [0, 0],
    className: 'animated-icon',
    html: html
  });
}

export default function constructMap(cb) {
  let map = null;

  const states = [];
  const cities = [];

  function generateStates(data) {
    // Call for each country state
    const areas = [];
    turf.featureEach(data, (feature) => {
      areas.push(turf.area(feature));
    });
    const maxArea = Math.max.apply(null, areas);

    turf.featureEach(data, (feature) => {
      const state = {id: feature.id, points: []};
      const bbox = turf.bbox(turf.getGeom(feature));

      const maxSeed = Math.floor(turf.area(feature) / maxArea * MAX_SEED_STATE_POINT_NUMBER);

      const citiesInPoly = [];

      cities.forEach((city) => {
        if (turf.pointsWithinPolygon(city, feature).features.length) {
          citiesInPoly.push(city);
        }
      });

      citiesInPoly.forEach((city) => {
        state.points.push(city);
      });

      while (state.points.length < maxSeed) {
        const point = turf.randomPoint(1, {bbox: bbox}).features[0];
        const pointsInPoly = turf.pointsWithinPolygon(point, feature);

        if (pointsInPoly.features.length) {
          const minDistance = Math.min.apply(null, state.points.map((_) => turf.distance(point, _)));
          // console.debug('Min distance', minDistance);
          if (minDistance > MAX_SEED_CITY_POINT_DISTANCE) {
            state.points.push(point);
          } else {
            console.debug('constructMap#generateStates Too near');
          }
        } else {
          console.debug('constructMap#generateStates Not in poly');
        }
      }

      states.push(state);
    });
  }

  function markerForPoint(point) {
    const coord = L.latLng(turf.getCoord(turf.flip(point)));
    const icon = iconForMarker();
    const marker = L.marker(coord, {
      icon: icon,
      title: point.properties.name,

      color: `#FF0000`,
      opacity: 0.5,
      radius: 2,
      weight: 2,

      start: {
        radius: 2
      },
      end: {
        radius: 20
      },

      transform: 'scale(2)'
    });

    marker.addTo(map);

    point.marker = marker;
  }

  function loadCities(cb) {
    $.getJSON('https://raw.githubusercontent.com/AshKyd/leaflet-geojson-map-boilerplate/master/data/australia.cities.geo.json', (data) => {
      cities.push.apply(cities, data.features.slice(0, 10));
      cb();
    });
  }

  function initialize(data) {
    $('.gallery__map_centered_text').before('<div id="map"></div>');

    map = L.map('map', {
      attributionControl: false,
      zoomControl: false,
      scrollWheelZoom: false
    });

    const geoJSON = L.geoJSON(data, {
      style: function () {
        return {
          color: '#F7F7F7',
          weight: 0,
          fillOpacity: 1
        };
      }
    });

    geoJSON.addTo(map);

    const bounds = geoJSON.getBounds();
    map.setMaxBounds(bounds);
    map.fitBounds(bounds);

    const currentZoom = map.getZoom();
    map.setMinZoom(currentZoom - 1);
    map.setMaxZoom(currentZoom + 1);

    states.forEach((state) => {
      state.points.forEach((point) => {
        markerForPoint(point);
      });
    });

    map.invalidateSize(true);

    cb(map, states);
  }

  $.getJSON('http://rowanhogan.github.io/australian-states/states.min.geojson', (data) => {
    loadCities(() => {
      generateStates(data);
      initialize(data);
    });
  });
}