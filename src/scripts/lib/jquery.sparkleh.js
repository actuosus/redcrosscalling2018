import Sparkle from './sparkle';
import $ from 'jquery';

/**
 * @author Arthur Chafonov <actuosus@gmail.com>
 * @fileoverview
 * @version 1.0.0
 * @module
 */

$.fn.sparkleh = function (options) {
  return this.each(function (k, v) {
    var $this = $(v).css('position', 'relative');

    var settings = $.extend({
      width: $this.outerWidth(),
      height: $this.outerHeight(),
      color: '#FFFFFF',
      count: 30,
      overlap: 0,
      speed: 1
    }, options);

    var sparkle = new Sparkle($this, settings);

    $this.on({
      'mouseover focus load': function (e) {
        sparkle.over();
      }//,
      /*'mouseout blur': function(e) {
          sparkle.out();
      }*/
    });
  });
};