/**
 * @author Arthur Chafonov <actuosus@gmail.com>
 * @fileoverview Utility functions declaration
 * @version 1.0.0
 * @module
 */

import * as turf from '@turf/turf/index';
import L from 'leaflet';

export function randomInteger(min, max) {
  var rand = min + Math.random() * (max + 1 - min);
  rand = Math.floor(rand);
  return rand;
}

export function delay(fn, ms) {
  // private instance variables
  let queue = [],
    self, timer;

  function schedule(f, t) {
    timer = setTimeout(function () {
      timer = null;
      f();
      if (queue.length > 0) {
        var item = queue.slice(0, 1)[0];
        queue = queue.slice(1);
        schedule(item.fn, item.ms);
      }
    }, t);
  }

  self = {
    delay: function (f, t) {
      if (queue.length > 0 || timer) {
        queue = queue.concat([{
          fn: f,
          ms: t
        }]);
      } else {
        schedule(f, t);
      }
      return self;
    }
  };

  return self.delay(fn, ms);
}

export function getRandomPoint() {
  const points = states.reduce((acc, state) => acc.concat(state.points), []);

  const statePoint = points[Math.floor(Math.random() * points.length)];
  const coord = turf.getCoord(statePoint);

  statePoint.marker.start();
  const point = map.latLngToLayerPoint(L.latLng(coord[1], coord[0]));
  return {x: Math.floor(point.x), y: Math.floor(point.y)};
}