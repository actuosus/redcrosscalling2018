/**
 * @author Arthur Chafonov <actuosus@gmail.com>
 * @fileoverview Main application code declaration
 * @version 1.0.0
 * @module main
 */

/*jslint white: true, single: true, node: true */
/*jshint esversion: 6 */

import 'reset-css/reset.css';
import '../styles/modal.css';
import '../styles/gallery.css';
import '../styles/main.css';

import $ from 'jquery';
import Clipboard from 'clipboard';
import moment from 'moment';
import inView from 'in-view';

import * as turf from '@turf/turf';

import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import {randomInteger, delay} from './lib/utils';
import './lib/jquery.sparkleh';
import constructMap, {ICON_GROW_STEP, iconForMarker} from './modules/map';


const ICON_GROW_DELAY = 700;
const ICON_SIZE_WITH_TEXT = 16;

$(function () {
  $('.gallery--theme-cc_tree, .gallery--theme-cc_tree1440 ').sparkleh({
    count: 235,
    color: ['#f3edc4', '#253943', '#659e3f'],
    speed: 0.4
  });

  let states = [];
  let map = null;

  constructMap((_, st) => {
    map = _;
    states = st;
  });

  const apiUrl = API_URL;

  let rotateImageInterval = null;
  const theme = 'red_cross';
  if (theme === 'red_cross') {
    var mapPoints = [{
      'id': '5a70bb777bb777837f3bfaf2',
      'city': 'TAS',
      'lat': 0,
      'long': 0,
      'count': 4
    }, {
      'id': '5a70bb777bb777837f3bfaf0',
      'city': 'NSW',
      'lat': 0,
      'long': 0,
      'count': 18
    }, {
      'id': '5a70bb777bb777837f3bfaee',
      'city': 'ACT',
      'lat': 0,
      'long': 0,
      'count': 16
    }, {
      'id': '5a70bb777bb777837f3bfaf1',
      'city': 'WA',
      'lat': 0,
      'long': 0,
      'count': 14
    }, {
      'id': '5a70bb777bb777837f3bfaef',
      'city': 'VIC',
      'lat': 0,
      'long': 0,
      'count': 16
    }, {
      'id': '5a70bb777bb777837f3bfaf3',
      'city': 'SA',
      'lat': 0,
      'long': 0,
      'count': 8
    }, {
      'id': '5a70bb777bb777837f3bfaf4',
      'city': 'QLD',
      'lat': 0,
      'long': 549.3418,
      'count': 14
    }];
  } else {
    var mapPoints = [];
  }
  var lastId;
  var imageQueue = [];
  var lastDt = '2018-02-01T00:56:11+00:00';
  var isAnimating = false; // true if we run animation cycle

  var gallery = $('.gallery');
  var overlay = $('.overlay');
  var overlayCard = $('.overlay__card');

  var tooltip = $('#tooltip');

  function showTooltip(text) {
    tooltip._.set({
      style: {
        visibility: 'visible'
      },
      contents: text
    });

    setTimeout(function () {
      tooltip._.style({
        visibility: 'hidden'
      });
    }, 2000);
  }

  // show tooltip on successful clipboard copy
  var clipboards = new Clipboard('.clipboard');
  clipboards.on('success', function (e) {
    showTooltip('Copied');
    e.clearSelection();
  });

  // only load images in viewport
  inView('.image').on('enter', function (e) {
    if (!e.classList.contains('image--loaded')) {
      e.src = e.getAttribute('data-src');
      e.classList.add('image--loaded');
    }
  });

  function showNewItem() {
    var newItem = $('.gallery__item--new');
    if (newItem !== null) {
      isAnimating = false;
      newItem.removeClass('gallery__item--new');
    }
  }

  function setRotateInterval() {
    console.log('rotate');
    rotateImageInterval = setInterval(function () {
      showRandomImage();
    }, 10000);
  }

  function clearRotateInterval() {
    clearInterval(rotateImageInterval);
  }

  function getRandomPoint() {
    const points = states.reduce((acc, state) => acc.concat(state.points), []);

    const statePoint = points[Math.floor(Math.random() * points.length)];
    const coord = turf.getCoord(statePoint);

    const currentIconElement = statePoint.marker._icon;

    currentIconElement.classList.add('pulse');

    setTimeout(() => {
      currentIconElement.classList.remove('pulse');
      const currentIconSize = statePoint.marker.options.icon.options.iconSize[0];
      const iconSize = currentIconSize + ICON_GROW_STEP;
      const currentValue = parseInt(currentIconElement.innerHTML, 10) || 0;
      let html = currentValue || '';
      if (iconSize >= ICON_SIZE_WITH_TEXT) {
        html = (currentValue || Math.floor(Math.random() * 100)) + 1;
      }
      statePoint.marker.setIcon(iconForMarker(iconSize, html));
    }, ICON_GROW_DELAY);

    const point = map.latLngToLayerPoint(L.latLng(coord[1], coord[0]));
    return {x: Math.floor(point.x), y: Math.floor(point.y)};
  }

  function showRandomImage() {
    var random = randomInteger(0, $('.gallery__item').length),
      $randEl = $('.gallery__item').eq(random),
      img = $randEl.find('img').first();

    delay(previewEnter, 1000)
      .delay(previewLeave, 5000)
      .delay(previewReset, 500);

    overlayCard.get(0).style.transform = '';
    overlayCard.find('.overlay__image').attr('src', img.attr('data-src'));

    const item = img.data('item');
    const $overlayMeta = $('.overlay__meta');

    if (!$.isEmptyObject(item)) {
      $overlayMeta
        .removeClass('hidden')
        .html(`<a href="" class="overlay__meta__name">${item.extraData.name}</a> from <span class="overlay__meta__city">${item.extraData.city}</span>`);
    } else {
      $overlayMeta.addClass('hidden');
    }
    isAnimating = true;
  }

  function preloadImage(url, cb) {
    // FIXME: http://blissfuljs.com/docs.html#fn-create
    var image = new Image();
    var retries = 0;

    var unbindEvents = function () {
      image.onload = null;
      image.onerror = null;
      image.onabort = null;
    };

    var onLoad = function () {
      unbindEvents();
      cb();
    };

    var onError = function () {
      setTimeout(function setImgSrc() {
        if (retries === 10) {
          unbindEvents();
          // FIXME: hardcoded structure
          image.parentNode.parentNode.removeChild(image.parentNode);
          isAnimating = false;
        }
        if (theme !== 'cc_tree') {
          image.src = url;
        } else {
          image.src = '//res.cloudinary.com/interlikeapp/image/fetch/c_scale,fl_png8.preserve_transparency,r_max,w_270/' + window.location.origin + url;
        }
        retries += 1;
      }, 1000);
    };

    image.onload = onLoad;
    image.onerror = onError;
    image.onabort = onError;

    if (theme !== 'cc_tree') {
      image.src = url;
    } else {
      image.src = '//res.cloudinary.com/interlikeapp/image/fetch/c_scale,fl_png8.preserve_transparency,r_max,w_270/' + window.location.origin + url;
    }
    return image;
  }

  function previewEnter() {
    if (theme !== 'cc_tree') {
      overlay.removeClass('overlay--enter');
      overlay.addClass('overlay--show');
    }
  }

  function previewLeave() {
    if (theme !== 'cc_tree') {
      const position = getRandomPoint();
      overlay.removeClass('overlay--show');
      const scale = 0.05;
      const translatePosition = {
        x: position.x - $('.gallery').width() / 2 - (overlayCard.width() / 2 * scale),
        y: position.y - $('.gallery').height() / 2 - (overlayCard.height() / 2 * scale)
      };
      overlayCard.get(0).style.transform = `translate(${translatePosition.x}px, ${translatePosition.y}px) scale(${scale})`;
      overlay.addClass('overlay--leave');
    }
  }

  function previewReset() {
    if (theme !== 'cc_tree') {
      overlay.removeClass('overlay--leave');
      overlay.addClass('overlay--enter');
    }
    if (theme === 'red_cross') {
      overlayCard.attr('class', 'overlay__card overlay__card-red_cross');
    }
    showNewItem();
  }

  function addImage(id, url, item) {
    var img = preloadImage(url, function () {
      delay(previewEnter, 1000)
        .delay(previewLeave, 5000)
        .delay(previewReset, 500);
    });
    img.classList.add('image', 'gallery__image');
    img.setAttribute('data-id', id);
    $(img).data('item', item);
    var div = document.createElement('div'/*, {
                  className: 'gallery__item gallery__item--new',
                  contents: [img]
              }*/);
    div.className = 'gallery__item gallery__item--new';
    div.appendChild(img);

    console.log('out photo');

    if (theme !== 'cc_tree') {
      gallery.prepend(div);
    } else {
      $('.gallery__item').last().after(div);
    }
    overlayCard.attr('src', CLOUDINARY_URL + (CLOUDINARY_HOST || window.location.origin) + url);
    isAnimating = true;
  }

  function incrementCity(city) {
    mapPoints = mapPoints.map(function (val, index, array) {
      console.log(val.city == city, val.city, city);
      if (val.city == city) {
        if (lastId !== val.id) {
          console.log(lastId, val.id);
          overlayCard.addClass('overlay__image--' + city);

          lastId = val.id;
          val.count++;
          $('.gallery__map_point_' + val.city).width(val.count).height(val.count).addClass('pulse');
          setTimeout(function () {
            $('.gallery__map_centered_text__red').text(parseInt($('.gallery__map_centered_text__red').text()) + 1).addClass('shaking');

            setTimeout(function () {
              $('.gallery__map_point_' + val.city).removeClass('pulse');
              $('.gallery__map_centered_text__red').removeClass('shaking');
            }, 600);
          }, 6500);
        }
      }
      return val;
    });
  }

  function checkQueue() {
    if (isAnimating) {
      return;
    }

    if (imageQueue.length > 0) {

      if (theme === 'red_cross') {
        clearRotateInterval();
      }
      var item = imageQueue.slice(0, 1)[0];
      imageQueue = imageQueue.slice(1);
      addImage(item.id, item.url, item);

      if (theme === 'red_cross') {
        if (item.extraData) {
          incrementCity(item.extraData.city);
        }
        setRotateInterval();
      }
    }
  }

  function maxDate(acc, v) {
    if (acc === null) {
      return moment(v.createDate);
    } else {
      return moment(v.createDate).isAfter(acc) ? v.createDate : acc;
    }
  }

  function checkGallery() {
    var url = apiUrl;
    if (lastDt) {
      url += '?dt=' + lastDt;
    }
    $.get(url).done(function (xhr) {
        try {
          var data = xhr;
          console.log('success');

        } catch (e) {
          console.log(e);
          var data = [];
        }
        lastDt = data.reduce(maxDate, lastDt);
        imageQueue = imageQueue.concat(
          data
            .filter(function imageNotInGallery(v) {
              return $(`.image[data-id="${v._id}"]`).length > 0;
            })
            .map(function (v) {
              return {
                id: v._id,
                url: v.url,
                extraData: v.extraData
              };
            })
        );
      })
      .fail(function (err) {
        // just silence
      });
  }

  // pause between images should be sum of
  // .gallery__item translation time along with delay
  // .overlay__image translation time along with delay
  // timeouts for previewEnter and previewLeave
  setInterval(checkQueue, 7000);
  setInterval(checkGallery, 10000);
  if (theme === 'red_cross') {
    setRotateInterval();
  }
});